var menu = new Vue({
    el: '#appIndex',
    data: {
        categorias: [],
        articulos: [],
        totalCom: [],
        id: '',
        comentarios: [],
        nombre: '',
        categoria: '',
        contexto: '',
        autor: '',
        comentario: '',
        nuevos: ''
    },
    methods: {
        btnAgrega: async function(){                    
            const {value: formValues} = await Swal.fire({
                title: 'Agregar Articulo',
                html:
                    '<div class="form-group">'+
                        '<div class="row">'+
                            '<label class="col-sm-3 col-form-label">Nombre del Artículo</label>'+
                            '<div class="col-sm-7">'+
                                 '<input id="nombre" type="text" class="form-control">'+
                            '</div>'+
                        '</div><br>'+
                        '<div class="row">'+
                            '<label class="col-sm-3 col-form-label">Categoria</label>'+
                            '<div class="col-sm-7">'+
                                 '<input id="categoria" type="text" class="form-control">'+
                            '</div>'+
                        '</div><br>'+
                        '<div class="row">'+
                            '<label class="col-sm-3 col-form-label">Contexto del Artículo</label>'+
                            '<div class="col-sm-7">'+
                                '<textarea class="form-control" id="contexto" rows="3"></textarea>'+
                            '</div>'+
                        '</div><br>'+
                        '<div class="row">'+
                            '<label class="col-sm-3 col-form-label">Autor</label>'+
                            '<div class="col-sm-7">'+
                                 '<input id="autor" type="text" class="form-control">'+
                            '</div>'+
                        '</div>'+
                    '</div>',        
                focusConfirm: false,
                showCancelButton: true,
                confirmButtonText: 'Guardar',           
                confirmButtonColor:'#00AA00',         
                cancelButtonColor:'#A6A2A2',  
                preConfirm: () => {            
                    return [
                        this.nombre = document.getElementById('nombre').value,
                        this.categoria = document.getElementById('categoria').value,
                        this.contexto = document.getElementById('contexto').value,
                        this.autor = document.getElementById('autor').value                 
                    ]
                }
            })        
            if(this.nombre == "" || this.categoria == "" || this.contexto == "" || this.autor == ""){
                Swal.fire({
                    type: 'info',
                    title: 'Datos incompletos',                                    
                }) 
            } else {          
                this.agregarArticulo();               
            }
        },
        btnComentario: async function(id){                    
            const {value: formValues} = await Swal.fire({
                title: 'Verificación',
                html:
                    '<div class="form-group">'+
                        '<div class="row">'+
                            '<label class="col-sm-5 col-form-label">Nombre del Usuario</label>'+
                            '<div class="col-sm-7">'+
                                '<input id="nombre" type="text" class="form-control">'+
                            '</div>'+
                        '</div><br>'+
                        '<div class="row">'+
                            '<label class="col-sm-5 col-form-label">Comentario</label>'+
                            '<div class="col-sm-7">'+
                                '<input id="comentario" type="text" class="form-control">'+
                            '</div>'+
                        '</div>'+
                    '</div>',        
                focusConfirm: false,
                showCancelButton: true,
                confirmButtonText: 'Guardar',          
                confirmButtonColor:'#00AA00',          
                cancelButtonColor:'#A6A2A2',  
                preConfirm: () => {            
                    return [
                        this.id = id,
                        this.nombre = document.getElementById('nombre').value,
                        this.comentario = document.getElementById('comentario').value                 
                    ]
                }
            })        
            if(this.id == "" || this.comentario == ""){
                Swal.fire({
                    type: 'info',
                    title: 'Datos incompletos',                                    
                }) 
            } else {          
                this.agregarComentario();               
            }   
        },
        listaCategorias: function(){
            axios.post('bd/seeInfo.php', { opcion:1 }).then(response =>{           
                    
                this.categorias = response.data;
                /*console.log(this.mensajes);
                this.total = Math.ceil(response.data.length);*/

            });
        },
        listaArticulos: function(){
            axios.post('bd/seeInfo.php', { opcion: 2 }).then(response =>{           
                    
                this.articulos = response.data;
                /*console.log(this.mensajes);
                this.total = Math.ceil(response.data.length);*/

            });
        },
        listaComentarios: function(){
            axios.post('bd/seeInfo.php', { opcion: 3 }).then(response =>{           
                    
                this.comentarios = response.data;
                /*console.log(this.mensajes);
                this.total = Math.ceil(response.data.length);*/

            });
        },
        agregarArticulo: function(){
            axios.post('bd/insert.php', { opcion: 1, nombre: this.nombre, categoria: this.categoria, contexto: this.contexto, autor: this.autor }).then(response =>{           
                
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    type: 'success',
                    title: '¡Artículo Agregado!'
                })
                this.listaArticulos();
                this.listaComentarios();
            });
        },
        agregarComentario: function(){
            axios.post('bd/insert.php', { opcion: 2, id: this.id, nombre: this.nombre, comentario: this.comentario }).then(response =>{           
                
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    type: 'success',
                    title: '¡Artículo Agregado!'
                })
                this.listaArticulos();
                this.listaComentarios();
            });
        },
        nuevo: function(){
            var nue = "nue";
            axios.post('bd/seeInfo.php', { opcion: 4, nuevo: nue }).then(response =>{
                this.articulos = response.data;
            });
        },
        todos: function(){
            this.listaArticulos();
            this.listaComentarios();
        },
        catego: function(categoria){
            axios.post('bd/seeInfo.php', { opcion: 5, catego: categoria }).then(response =>{
                this.articulos = response.data;
            });
        },
    },
    created: function(){   

        this.listaCategorias();
        this.listaArticulos();
        this.listaComentarios();

    }, 
});