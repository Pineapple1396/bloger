<?php
    session_start();

    include_once 'conexion.php';
    $objeto = new Conexion();
    $conexion = $objeto->Conectar();

    $_POST = json_decode(file_get_contents("php://input"), true);
    $opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
    $nue = (isset($_POST['nue'])) ? $_POST['nue'] : '';
    $categoria = (isset($_POST['catego'])) ? $_POST['catego'] : '';

    switch($opcion){         
        case 1:
            $consulta = "SELECT DISTINCT categoria FROM articulos";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        case 2:
            $consulta = "SELECT * FROM articulos";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        case 3:
            $consulta = "SELECT * FROM comentarios";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        case 4:
            $consulta = "SELECT * FROM articulos ORDER BY id DESC ";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        case 5:
            $consulta = "SELECT * FROM articulos WHERE categoria='$categoria'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    }
    print json_encode($data, JSON_UNESCAPED_UNICODE);
    $conexion = NULL;