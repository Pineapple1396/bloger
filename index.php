<!DOCTYPE html>
    <html>
        <?php session_start(); ?>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <title>Blog</title>

            <!-- Bootstrap CSS -->    
            <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
            <!-- FontAwesom CSS -->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">        
            <!--Sweet Alert 2 -->
            <link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
            <!-- Our Custom CSS -->
            <link rel="stylesheet" href="css/navbar.css">

        </head>

        <body>
            <div class="wrapper" id="appIndex">
                <!-- Sidebar Holder -->
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <h3>Articulos</h3>
                    </div>

                    <ul class="list-unstyled components">
                        <li>
                            <a @click="btnAgrega()">Agregar Articulo</a>
                        </li>
                        <li>
                            <a @click="todos()">Todos</a>
                        </li>
                        <li>
                            <a @click="nuevo()">Nuevos</a>
                        </li>
                        <li class="active">
                            <a href="#cate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Categorias</a>
                            <ul class="collapse list-unstyled" id="cate">
                                <li v-for="(cat, indice) of categorias">
                                    <a @click="catego(cat.categoria)">{{ cat.categoria }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>

                <!-- Page Content Holder -->
                <div id="content">
                    <div class="row">
                        <div class="col-4">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fas fa-align-justify"></i>
                            </button>
                        </div>
                        <div class="col-8">
                            <h1>Infotek.org</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div  v-for="(art, indice) of articulos">
                                <div class="card text-white bg-secondary">
                                    <div class="card-header"><h2>{{ art.nombre }}</h2></div>
                                    <div class="card-body">
                                        <h5 class="card-title">Escrito por {{  art.autor }}, {{ art.fecha }}</h5>
                                        <p class="card-text">{{ art.contexto }}</p>
                                    </div>
                                    <div class="card-header">
                                        <h5>Comentarios: </h5>
                                        <div>
                                            <div v-for="(com, indice) in comentarios" v-show="art.id == com.id_articulo">
                                                <a>{{ com.nombre }} respondio: {{ com.comentario }}, {{ com.fecha }}</a><br>
                                            </div><br>
                                            <div class="form-group row">
                                                <button type="button" @click="btnComentario(art.id)" class="btn btn-dark mb-2">Comentar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    
            <!-- jQuery, Popper.js, Bootstrap JS -->
            <script src="jquery/jquery-3.3.1.min.js"></script>
            <script src="popper/popper.min.js"></script>
            <script src="bootstrap/js/bootstrap.min.js"></script>      
            <!--Vue.JS -->    
            <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->
            <script src="https://unpkg.com/vue/dist/vue.js"></script>
            <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>              
            <!--Axios -->      
            <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>    
            <!--Sweet Alert 2 -->        
            <script src="plugins/sweetalert2/sweetalert2.all.min.js"></script>
            <script src="js/index.js"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('#sidebarCollapse').on('click', function () {
                        $('#sidebar').toggleClass('active');
                        $(this).toggleClass('active');
                    });
                });
                $( "#notifi" ).click( function() {
                    $( "#caciones" ).toggle();
                });
            </script>
        </body>
    </html>
