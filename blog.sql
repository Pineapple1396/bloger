-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 14-01-2021 a las 04:54:09
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

DROP TABLE IF EXISTS `articulos`;
CREATE TABLE IF NOT EXISTS `articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(150) NOT NULL,
  `nombre` text NOT NULL,
  `contexto` text NOT NULL,
  `autor` varchar(150) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `categoria`, `nombre`, `contexto`, `autor`, `fecha`) VALUES
(1, 'Tecnología', '¿Qué papel ocupan las TIC en la educación actual?', 'La neuroeducación, junto con el uso de las TIC en la educación, se sitúan como las herramientas clave para el desarrollo de la enseñanza. Pero, ¿qué piensa al respecto uno de los grandes pensadores del siglo XXI?\r\n\r\nNiall Ferguson, historiador, escritor y educador, acudió al EnlightED 2019, el encuentro internacional de educación, que este año se celebrará entre el 19 y 23 de octubre; para hablar de los retos de la educación en la era digital.', 'Blanca Montoya Gago', '2021-01-13'),
(2, 'Tecnología', 'IFA 2020: las novedades que esperamos en su edición on-line', 'Se esperan muchas novedades en este IFA 2020 marcado por el contexto del COVID19. La exposición se realizará físicamente en Berlín, pero no estará abierta al público general.\r\n\r\nDiego de la Torre\r\n \r\nETIQUETAS: IFA, LG, SAMSUNG, TECNOLOGÍA\r\nLa exposición tecnológica más grande de Europa también ha sufrido los efectos de la COVID-19. La IFA 202 Special Edition tendrá lugar en Berlín entre el 3 y 5 de septiembre, pero con algunas novedades. Y es que, por razones de salud pública y seguridad, por primera vez el recinto ferial estará cerrado al público en general.\r\n\r\nEste año, los organizadores han decidido que la IFA 2020 Special Edition presencial será un evento B2B puro al que tan solo podrán acceder profesionales y prensa. Además, el acceso estará limitado a 1.000 personas diarias y el recinto se dividirá en un escenario principal donde se harán todas las presentaciones y unos stands de marcas solo para periodistas.', 'Diego de la Torre', '2021-01-13'),
(3, 'Innovación', 'Innovaciones tecnolígias que han marcado el desarrollo de la economí­a mundial', 'Año tras año, surgen nuevas tecnologí­as. Algunas se convierten en innovaciones disruptivas, pues cambian la manera en la que las organizaciones y las industrias funcionan. Obligan a los negocios a alterar sus operaciones para no perder cuota de mercado o volverse irrelevantes.', 'Moncho Terol', '2021-01-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_articulo` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `comentario` text NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `id_articulo`, `nombre`, `comentario`, `fecha`) VALUES
(1, 1, 'José', 'Muy buen articulo', '2021-01-13 10:48:31'),
(2, 1, 'Esteban', 'Le falta más información', '2021-01-13 11:35:09'),
(3, 2, 'Sonia', 'Le falta más información', '2021-01-13 13:39:08'),
(4, 3, 'Sebastian', 'Muy Bueno', '2021-01-13 22:13:54');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
